import { readFile, writeFile } from 'fs/promises';
import { promisify } from 'util';
import { exec as _exec } from 'child_process';

// Promisify exec
const exec = promisify(_exec);

(async function main() {
  // keys to replace in package.json
  const keys = ['name', 'description', 'repository', 'homepage'];

  const packageJson = JSON.parse(await readFile('package.json', 'utf8'));
  
  const currentProjectName = packageJson.name;
  
  console.log(`Current project name: ${currentProjectName}`);

  const projectUri = await exec('git remote get-url origin')
  // Check that the command was successful
  if (projectUri.stderr) {
    throw new Error(projectUri.stderr);
  }

  const newProjectName = projectUri.stdout
    .split('/')
    .pop()
    .replace('.git\n', '');

  for (const key of keys) {
    packageJson[key] = packageJson[key].replace(currentProjectName, newProjectName);
  }

  await writeFile('package.json', JSON.stringify(packageJson, null, 2));

  console.log(`New project name: ${newProjectName}`);
})()
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });