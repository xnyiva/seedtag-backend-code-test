import logRequest from './logRequest';

// Setup a mock context object
const ctx: any = {
  log: {
    info: jest.fn(),
  },
  set: jest.fn(),
  request: {
    method: 'GET',
    path: '/test',
    query: {},
    body: {},
  },
};

// Setup a mock next function
const next = jest.fn(() => {
  ctx.status = 200;
});

describe('logRequest', () => {

  test('is a function', () => {
    expect(logRequest).toBeInstanceOf(Function);
  });

  test('calls next once', async () => {
    // Execute the middleware
    await logRequest(ctx, next);
    expect(next).toHaveBeenCalledTimes(1);
  });

  test('sets the X-Response-Time header', async () => {
    expect(ctx.set).toHaveBeenCalledTimes(1);
    expect(ctx.set).toHaveBeenCalledWith(
      'X-Response-Time',
      expect.any(String),
    );
  });

  test('logs the request start and end', async () => {
    expect(ctx.log.info).toHaveBeenCalledTimes(2);
    expect(ctx.log.info).toHaveBeenCalledWith(
      'req-start',
      {
        reqId: expect.any(String),
        start: expect.any(Number),
        method: 'GET',
        path: '/test',
        query: {},
        body: {},
      },
    );
    expect(ctx.log.info).toHaveBeenCalledWith(
      'req-end',
      {
        reqId: expect.any(String),
        start: expect.any(Number),
        end: expect.any(Number),
        ms: expect.any(Number),
        method: 'GET',
        path: '/test',
        query: {},
        body: {},
        status: expect.any(Number),
      },
    );    
  });
});
