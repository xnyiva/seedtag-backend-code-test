import { IMiddleware } from 'koa-router';

/**
 * Create a middleware function that throws a 415 error if the Content-Type header does not match the given string
 * @param contentType A string to match the Content-Type header against
 * @returns A middleware function that throws a 415 error if the Content-Type header does not match the given string
 */
export default function contentTypeIs(contentType = ''): IMiddleware {
  return async (ctx: any, next: Function) => {
    if (ctx.is(contentType)) {
      return next();
    }

    ctx.throw(415, `Content-Type must be '${contentType}'`);
  };
}