import handleError from './handleError';

// Make typescript happy
class ArbitraryError extends Error {
  [key: string]: any;
}

// Create a mocked context object
function createContext(): any {
  return {
    id: 'test',
    log: {
      error: jest.fn(),
    },
    app: {
      emit: jest.fn(),
    },
    request: {
      method: 'GET',
      path: '/test',
      query: {},
      body: {},
    },
  };
}

describe('handleError', () => {
  test('is a function', () => {
    expect(handleError).toBeInstanceOf(Function);
  });

  describe('when next throws a duplicated key mongodb error', () => {
    const ctx = createContext();

    const error = new ArbitraryError('Test error');

    const next = jest.fn(() => {
      error.name = 'MongoError';
      error.code = 11000;
      throw error;
    });

    test('calls next once', async () => {
      await handleError(ctx, next);

      expect(next).toHaveBeenCalledTimes(1);
    });
  
    test('logs the error', () => {
      expect(ctx.log.error).toHaveBeenCalledTimes(1);
      expect(ctx.log.error).toHaveBeenCalledWith(
        'Error: Test error',
        {
          ctx,
          error,
        },
      );
    });
  
    test('sets the status to 400', async () => {
      expect(ctx.status).toBe(400);
    });
  
    test('sets the body to the error', () => {
      expect(ctx.body).toEqual({
        error,
      });
    });
  });

  describe('when next throws an error without status', () => {
    const ctx = createContext();

    const error = new ArbitraryError('Test error');

    const next = jest.fn(() => {
      throw error;
    });

    test('calls next once', async () => {
      await handleError(ctx, next);

      expect(next).toHaveBeenCalledTimes(1);
    });
  
    test('logs the error', () => {
      expect(ctx.log.error).toHaveBeenCalledTimes(1);
      expect(ctx.log.error).toHaveBeenCalledWith(
        'Error: Test error',
        {
          ctx,
          error,
        },
      );
    });
  
    test('sets the status to 500', () => {
      expect(ctx.status).toBe(500);
    });
  
    test('sets the body to the error', () => {
      expect(ctx.body).toEqual({
        error,
      });
    });
  });

  describe('when running in production', () => {
    const ctx = createContext();

    const error = new ArbitraryError('Test error');

    const next = jest.fn(() => {
      throw error;
    });

    test('sets the body to a generic error', async () => {
      process.env.NODE_ENV = 'production';

      await handleError(ctx, next);

      expect(ctx.body).toEqual({
        error: 'Internal server error',
      });

      process.env.NODE_ENV = 'test';
    });
  });
});
