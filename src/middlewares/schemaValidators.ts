/* eslint-disable no-param-reassign */
import Ajv from 'ajv';
import addFormats from 'ajv-formats';
import get from 'lodash.get';
import set from 'lodash.set';

import { IMiddleware } from 'koa-router';

/**
 * Quick an dirty deep clone
 * @param {Object} obj object to be cloned (must not have any circular reference)
 * @returns {Object} the cloned object
 * @private
 */
function jsonClone(obj: object): any {
  return JSON.parse(
    JSON.stringify(obj),
  );
}

/**
 * Creates a Koa middleware that validates input from 'sourcePath'
 * and outputs validated data to 'destPath' of the context object
 * @param {Object} schema JSONSchema draft-07
 * @param {String} sourcePath path of ctx to validate
 * @param {String} destPath path of ctx to put the validated and parsed data to
 * @param {Object} ajvOptions options to pass to ajv
 * @returns {Function} A Koa middleware function
 */
function createValidator(
  schema: object,
  sourcePath: string = '',
  destPath: string = '',
  ajvOptions: any = { coerceTypes: true, useDefaults: true },
): IMiddleware {
  // Allow union types by default
  if (!Object
    .keys(ajvOptions)
    .find((key) => key === 'allowUnionTypes')
  ) {
    ajvOptions.allowUnionTypes = true;
  }

  // Instantiate ajv here to avoid creating a new instance for each request
  const ajv = new Ajv(ajvOptions);
  addFormats(ajv);
  
  /**
   * Validates input from 'sourcePath' and outputs validated data to 'destPath' of ctx
   * @param {Koa.Context} ctx the context object
   * @param {Function} next the next middleware in the chain
   * @returns {Promise<void>} a promise that resolves when the middleware finishes execution
   */
  return async function (ctx: any, next: Function): Promise<void> {
    const schemaValidator = ajv.compile(schema);
    const target = get(ctx, sourcePath);
    const targetClone = jsonClone(target);
    const valid = schemaValidator(targetClone);

    if (valid) {
      set(ctx, destPath, targetClone);
    } else {
      ctx.throw(400, 'Bad Request', { errors: schemaValidator?.errors });
    }

    await next();
  };
}

/**
 * Returns a middleware that validates ctx.request.body
 * @param {Object} schema JSONSchema draft-07
 * @param {Object} ajvOptions options to pass to ajv
 * @returns {Function} A Koa middleware function
 */
function body(
  schema: any,
  ajvOptions: object = { coerceTypes: true, useDefaults: true },
): IMiddleware {
  return createValidator(schema, 'request.body', 'valid.body', ajvOptions);
}

/**
 * Returns a middleware that validates ctx.params
 * @param {Object} schema JSONSchema draft-07
 * @param {Object} ajvOptions options to pass to ajv
 * @returns {Function} A Koa middleware function
 */
function params(
  schema: any,
  ajvOptions: object = { coerceTypes: true, useDefaults: true },
): IMiddleware {
  return createValidator(schema, 'params', 'valid.params', ajvOptions);
}

/**
 * Returns a middleware that validates ctx.query
 * @param {Object} schema JSONSchema draft-07
 * @param {Object} ajvOptions options to pass to ajv
 * @returns {Function} A Koa middleware function
 */
function query(
  schema: any,
  ajvOptions: object = { coerceTypes: true, useDefaults: true },
): IMiddleware {
  return createValidator(schema, 'query', 'valid.query', ajvOptions);
}

export {
  body,
  params,
  query,
  createValidator,
};
