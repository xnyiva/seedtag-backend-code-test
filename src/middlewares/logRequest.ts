import reqId from '../lib/reqId';

/**
 * Logs the request start and end
 * @param {Koa.Context} ctx the context object
 * @param {Function} next the next middleware in the chain
 * @returns {Promise<void>} a promise that resolves when the middleware is done
 */
async function logRequest(ctx: any, next: Function): Promise<void> {
  // Get the start time
  const start = Date.now();

  // Get the logger from the context
  const { log } = ctx;

  // Get the request details
  const {
    method,
    path,
    query,
    body,
  } = ctx.request;

  // Generate a unique request id and attach it to the context
  const id = reqId();
  ctx.reqId = id;

  // Log the request start
  const reqLog = {
    reqId: ctx.reqId,
    start,
    method,
    path,
    query,
    body,
  };
  log.info('req-start', reqLog);

  // Execute the rest of the middleware chain
  await next();

  // Log the request end
  const { status } = ctx;
  const end = Date.now();
  const ms = end - start;
  ctx.set('X-Response-Time', `${ms}ms`);
  log.info('req-end', {
    ...reqLog,
    end,
    ms,
    status,
  });
}

export default logRequest;
