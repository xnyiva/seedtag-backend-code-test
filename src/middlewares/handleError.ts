/**
 * Logs errors and sends them to the client (if not in production)
 * @param {Koa.Context} ctx The context object
 * @param {Function}next The next middleware in the chain
 */
async function handleError(ctx: any, next: Function): Promise<void> {
  try {
    // execute all middlewares
    await next();
    // catch any error that was not handled
  } catch (error: Error | any) {
    // catch error duplicated key in mongo
    if (
      error.code === 11000
    ) {
      error.status = 400;
    }

    ctx.log.error(`Error: ${error.message}`, { ctx, error });
    
    ctx.status = error.status || 500;
    ctx.body = process.env.NODE_ENV === 'production'
      // do not send error details in production
      ? { error: 'Internal server error' }
      // send them otherwise
      : { error };

    // emit the error to the app if not testing
    if (process.env.NODE_ENV !== 'testing') ctx.app.emit('error', error);
  }
}

export default handleError;
