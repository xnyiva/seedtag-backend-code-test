/* eslint-disable no-undef */
import {
  query,
  body,
  params,
  createValidator,
} from './schemaValidators';

// Utility function to call a middleware and mock Koa next function
async function callMiddleware(middleware: Function, ctx: any) {
  // Mock Koa next function
  const next = jest.fn();

  await middleware(ctx, next);

  return {
    ctx,
    next,
  };
}

// A simple schema to test with
const testSchema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  title: 'test schema',
  type: 'object',
  definitions: {
    typeop: {
      type: 'string',
      pattern: '^QUERY$|^OR$|^AND$|^NOT$',
    },
    id: {
      type: 'string',
      pattern: '[a-fA-F0-9]{24}',
    },
    number: {
      type: 'number',
      minimum: 1,
      maximum: 100,
    },
    uri: {
      type: 'string',
      format: 'uri',
    },
  },
  properties: {
    typeop: {
      $ref: '#/definitions/typeop',
    },
    id: {
      $ref: '#/definitions/id',
    },
    number: {
      $ref: '#/definitions/number',
    },
  },
  required: [
    'typeop',
    'id',
    'number',
  ],
  additionalProperties: false,
};

// Some test data to validate
const testData = {
  typeop: 'QUERY',
  id: '55746edc4f02c260548b46fb',
  number: '23',
};

describe('koa-ajv', () => {
  // Middleware functions to test
  let bodyMiddleware: Function;
  let paramsMiddleware: Function;
  let queryMiddleware: Function;

  describe('createValidator', () => {
    test('is a function', () => {
      expect(createValidator).toBeInstanceOf(Function);
    });

    test('returns a function', () => {
      expect(createValidator(testSchema)).toBeInstanceOf(Function);
    });

    test('middleware validates as expected', async () => {
      const validator = createValidator(testSchema, 'arbitrary.sourcePath', 'arbitrary.destPath');

      const { ctx, next } = await callMiddleware(validator, { arbitrary: { sourcePath: testData } });

      expect(ctx?.arbitrary?.destPath?.typeop).toBe(testData.typeop);
      expect(ctx?.arbitrary?.destPath?.id).toBe(testData.id);
      expect(ctx?.arbitrary?.destPath?.number).toBe(Number(testData.number));
      expect(next).toHaveBeenCalledTimes(1);
    });
  });

  describe('body', () => {
    test('is a function', () => {
      expect(body).toBeInstanceOf(Function);
    });

    test('returns a function', () => {
      expect(bodyMiddleware = body(testSchema)).toBeInstanceOf(Function);
    });

    test('middleware validates as expected', async () => {
      const { ctx, next } = await callMiddleware(bodyMiddleware, { request: { body: testData } });

      expect(ctx?.valid?.body?.typeop).toBe(testData.typeop);
      expect(ctx?.valid?.body?.id).toBe(testData.id);
      expect(ctx?.valid?.body?.number).toBe(Number(testData.number));
      expect(next).toHaveBeenCalledTimes(1);
    });
  });

  describe('params', () => {
    test('is a function', () => {
      expect(params).toBeInstanceOf(Function);
    });

    test('returns a function', () => {
      expect(paramsMiddleware = params(testSchema)).toBeInstanceOf(Function);
    });

    test('middleware validates as expected', async () => {
      const { ctx, next } = await callMiddleware(paramsMiddleware, { params: testData });

      expect(ctx?.valid?.params?.typeop).toBe(testData.typeop);
      expect(ctx?.valid?.params?.id).toBe(testData.id);
      expect(ctx?.valid?.params?.number).toBe(Number(testData.number));
      expect(next).toHaveBeenCalledTimes(1);
    });
  });

  describe('query', () => {
    test('is a function', () => {
      expect(query).toBeInstanceOf(Function);
    });

    test('returns a function', () => {
      expect(queryMiddleware = query(testSchema)).toBeInstanceOf(Function);
    });

    test('middleware validates as expected', async () => {
      const { ctx, next } = await callMiddleware(queryMiddleware, { query: testData });

      expect(ctx?.valid?.query?.typeop).toBe(testData.typeop);
      expect(ctx?.valid?.query?.id).toBe(testData.id);
      expect(ctx?.valid?.query?.number).toBe(Number(testData.number));
      expect(next).toHaveBeenCalledTimes(1);
    });
  });
});