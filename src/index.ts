// Module imports
import * as dotenv from 'dotenv';
import { Server } from 'http';
import Koa from 'koa';
import cors from '@koa/cors';
import bodyparser from 'koa-bodyparser';
// import { MongoMemoryServer } from 'mongodb-memory-server';
// import mongoose from 'mongoose';

// File imports
// import connectToDatabase from './lib/connectToDatabase';
// import models from './models';
import createLogger from './lib/logger';
import handleError from './middlewares/handleError';
import logRequest from './middlewares/logRequest';
import routes from './routes';

// Load the environment variables
dotenv.config();

// Environment variables
const {
  NODE_ENV = 'development',
  NODE_PORT = 9091,
  NODE_DEBUG_LEVEL = (NODE_ENV === 'development' ? 'debug' : 'info'),
  // MONGO_URI,
} = process.env;

// let mongoServer: MongoMemoryServer | null = null;

// Setup the logger
const log = createLogger(NODE_DEBUG_LEVEL);

/**
 * Starts the server
 * @returns A promise that resolves to the server instance
 * @throws An error if the server fails to start
 **/
async function main(): Promise<Server> {  
  // Create a new Koa application
  const app = new Koa();
  
  // Attach the logger to the app context
  app.context.log = log;

  // If a MONGO_URI is not provided, start a new in-memory MongoDB server
  // if (!MONGO_URI) {
  //   log.info('No MONGO_URI provided, starting a new in-memory MongoDB server');
  //   mongoServer = await MongoMemoryServer.create();
    
  //   app.context.mongoUri = mongoServer.getUri();
  // } else {
  //   app.context.mongoUri = MONGO_URI;
  // }

  // Connect to the database
  // connectToDatabase(app.context.mongoUri, log);

  // Attach the models to the app context
  // app.context.models = models;

  // Attach middlewares and routes to the app
  app
    .use(
      cors({ origin: '*' }),
    )
    .use(
      bodyparser(),
    )
    .use(
      // Log the request
      logRequest,
    )
    .use(
      // Custom error handler
      handleError,
    )
    .use(
      // Attach the routes to the app
      routes.routes(),
    )
    .use(
      // Attach the allowed methods to the app
      routes.allowedMethods(),
    );

  // Start the server
  const server: Server = app.listen(
    NODE_PORT, 
    () => log.info(`Server running in ${NODE_ENV} mode on port ${NODE_PORT}`),
  );

  // server.on('close', () => {
  //   // If a MongoDB server was started, stop it
  //   if (mongoServer) {
  //     log.info('Stopping MongoDB server');
  //     mongoServer.stop();
  //   }
  // });

  // Handle SIGINT and SIGTERM
  const gracefulShutdown = (signal: string) => {
    return () => {
      log.info(`${signal} received, closing server`);
      server.close();

      // Close the database connection
      // log.info('Closing database connection');
      // mongoose.connection.close();

      // Exit the process
      log.info('Exiting process');
      process.exit(0);
    };
  };
  process.on('SIGINT', gracefulShutdown('SIGINT'));
  process.on('SIGTERM', gracefulShutdown('SIGTERM'));

  // Return the server (for testing purposes)
  return server;
}

if (require.main === module) {
  // If this file is run directly, start the server
  main().catch(
    (error: Error) =>
      // Log the error
      log.error(`Error: ${error.message}`, { error })
      // Exit with error
      && process.exit(1),
  );
}

export default main;
