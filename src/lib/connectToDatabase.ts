import mongoose, { ConnectOptions } from 'mongoose';

/**
 * Connect to MongoDB
 * @param uri The MongoDB URI
 * @param log The logger
 * @param options The connection options
 * @returns void
 **/
function connectToDatabase(
  uri: string,
  log: any,
  options = <ConnectOptions>{
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
): void {
  // Set the strictQuery option to false to allow for partial updates
  mongoose.set('strictQuery', false);

  mongoose.connect(
    uri,
    options,
    (err) => {
      if (err) {
        log.error('Error connecting to MongoDB', err);
        process.exit(1);
      }
      log.info('Connected to MongoDB');
    },
  );  
}

export default connectToDatabase;
