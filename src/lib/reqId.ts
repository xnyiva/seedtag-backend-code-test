
const numbers: string = '0123456789';
const lower: string = 'abcdefghijklmnopqrstuvwxyz';
const upper: string = lower.toLocaleUpperCase();
const chars: string[] = [...numbers.split(''), ...lower.split(''), ...upper.split('')];

/**
 * Generates a random id of the specified length
 * @param {number} len The length of the generated id
 * @returns {string} The generated id
 */
function reqId(len = 6): string {
  let id = '';
  while (id.length < len) {
    id += chars[Math.floor(Math.random() * chars.length)];
  }
  return id;
}

export default reqId;
