import winston, { Logger } from 'winston';

/**
 * Creates a logger instance
 * @param {string} level the log level
 * @returns {winston.Logger} a logger instance
 **/
function createLogger(level: string): Logger {
  const { prettyPrint } = winston.format;
  const { Console } = winston.transports;

  const transports = [];
  
  if (process.env.NODE_ENV == 'development') {
    transports.push(
      new Console({
        level,
        format: prettyPrint({ colorize: true }),
      }),
    );
  } else if (process.env.NODE_ENV === 'testing') {
    transports.push(
      /* 
      new Console({
        level,
        format: prettyPrint({ colorize: true }),
      }),
      */
      // Uncomment the above statement and comment the one below to see the logs while testing
      new Console({
        level,
        silent: true,
      }),
    );
  } else {
    transports.push(
      new Console({ level }),
    );
  }

  // Create and return the logger
  return winston.createLogger({
    transports,
  });
}

export default createLogger;
