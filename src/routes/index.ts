import Router from 'koa-router';

// Import all the routers
import radar from './radar';

interface Routers {
  [key: string]: Router;
}

const routers: Routers = {
  // Add your routers here
  radar,
};

// Create a new router to mount all the other routers
const router = new Router();

// Mount all the routers on the main router
for (const routerName of Object.keys(routers)) {
  router.use(`/${routerName}`, routers[routerName].routes(), routers[routerName].allowedMethods());
}

export default router;
