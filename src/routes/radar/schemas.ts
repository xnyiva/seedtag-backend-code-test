// Import custom types
import {
  Protocol,
  EnemyType,
} from './yvh';

const radarScanSchema = {
  $defs: {
    protocol: {
      type: 'string',
      enum: Object.values(Protocol),
    },
    enemyType: {
      type: 'string',
      enum: Object.values(EnemyType),
    },
    coordinates: {
      type: 'object',
      properties: {
        x: { type: 'number' },
        y: { type: 'number' },
      },
      required: ['x', 'y'],
      additionalProperties: false,
    },
  },
  type: 'object',
  properties: {
    protocols: {
      type: 'array',
      items: { $ref: '#/$defs/protocol' },
    },
    scan: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          coordinates: { $ref: '#/$defs/coordinates' },
          enemies: {
            type: 'object',
            properties: {
              type: { $ref: '#/$defs/enemyType' },
              number: { type: 'number' },
            },
            required: ['type', 'number'],
            additionalProperties: false,
          },
          allies: { type: 'number' },
        },
        required: ['coordinates', 'enemies'],
        additionalProperties: false,
      },
    },
  },
  required: ['protocols', 'scan'],
  additionalProperties: false,
};

export {
  radarScanSchema,
};
