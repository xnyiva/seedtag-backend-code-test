import Router from 'koa-router';

// Validation schemas
import {
  radarScanSchema,
} from './schemas';

// Middlewares
import { body } from '../../middlewares/schemaValidators';
import validateContentType from '../../middlewares/validateContentType';

import {
  getResolvedObjective,
} from './middlewares';

const router = new Router();

const contentType = 'application/json';

router
  .post('/', validateContentType(contentType), body(radarScanSchema), getResolvedObjective);

export default router;
