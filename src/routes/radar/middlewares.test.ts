
// Load middlewares
import {
  getResolvedObjective,
} from './middlewares';

// Load the test data
import testCases from './test-cases.json';

interface IMockContext {
  valid: any;
  status: number;
  body: any;
  docs?: any
}

// Helpers
function getMockContext(body: any = {}): IMockContext {
  return {
    valid: {
      body,
    },
    status: 0,
    body,
  };
}

beforeAll(async () => {
});

afterAll(async () => {
});

describe('radar', () => {
  describe('getResolvedObjective', () => {
    test('should return the correct objective', async () => {
      for (const testCase of testCases) {
        const mockContext = getMockContext(testCase.request);
        const next = jest.fn();

        await getResolvedObjective(mockContext, next);

        expect(mockContext.status).toBe(200);
        expect(mockContext.body).toEqual(testCase.response);
        expect(next).toHaveBeenCalledTimes(1);
      }
    });
  });
});
