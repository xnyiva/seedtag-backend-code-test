import {
  RadarScan,
  Coordinates,
  resolveObjective,
} from './yvh';

/**
 * Gets the validated radar scan from the request body and resolves the objective
 * @param ctx Koa context
 * @param next Next middleware
 */
export async function getResolvedObjective(ctx: any, next: any) {
  const radarScan: RadarScan = ctx.valid.body;
  const objective: Coordinates = resolveObjective(radarScan);

  ctx.status = 200;
  ctx.body = objective;

  await next();
}
