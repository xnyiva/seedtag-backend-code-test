import { Server } from 'http';
import request from 'supertest';
import createApp from '../../index';

import testData from './test-cases.json';

let app: Server;

beforeAll(async () => {
  app = await createApp();
});

afterAll(() => {
  app.close();
});

describe('/radar', () => {
  describe('POST /radar', () => {
    test('should return status 400 if the sent data does not validate', async () => {
      const response = await request(app)
        .post('/radar')
        .send({
          foo: 'bar',
        });

      expect(response.status).toBe(400);
    });

    test('should return status 200 if the sent data validates', async () => {
      const response = await request(app)
        .post('/radar')
        .send(testData[0].request);

      expect(response.status).toBe(200);
    });

    test('should return the expected coordinates', async () => {
      for (const testCase of testData) {
        const response = await request(app)
          .post('/radar')
          .send(testCase.request);

        expect(response.body).toEqual(testCase.response);
      }
    });   
  });
});