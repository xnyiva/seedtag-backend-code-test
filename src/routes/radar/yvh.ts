// Types
enum Protocol {
  avoidMech = 'avoid-mech',
  avoidCrossfire = 'avoid-crossfire',
  closestEnemies = 'closest-enemies',
  furthestEnemies = 'furthest-enemies',
  assistAllies = 'assist-allies',
  prioritizeMech = 'prioritize-mech',
}

enum EnemyType {
  mech = 'mech',
  soldier = 'soldier',
}

interface Enemy {
  type: EnemyType,
  number: number,
}

interface Coordinates {
  x: number,
  y: number,
}

interface Scan {
  coordinates: Coordinates,
  enemies: Enemy,
  allies?: number, 
}

interface RadarScan {
  protocols: Protocol[],
  scan: Scan[],
}

// Helpers
function getDistanceFromZero(coordinates: Coordinates): number {
  return Math.sqrt(coordinates.x ** 2 + coordinates.y ** 2);
}

function distanceLessThan(coordinates: Coordinates, distance: number = 100): boolean {
  return getDistanceFromZero(coordinates) < distance;
}

function hasAllies(scan: Scan): boolean {
  return scan.allies !== undefined;
}

function hasMech(scan: Scan): boolean {
  return scan.enemies.type === EnemyType.mech;
}

function sortByDistanceAsc(scan: Scan[]): Scan[] {
  return scan.sort((a, b) =>
    getDistanceFromZero(a.coordinates) - getDistanceFromZero(b.coordinates),
  );
}

function sortByDistanceDesc(scan: Scan[]): Scan[] {
  return scan.sort((a, b) =>
    getDistanceFromZero(b.coordinates) - getDistanceFromZero(a.coordinates),
  );
}

/**
 * Resolves the objective coordinates based on the radar scan
 * @param radarScan Radar scan
 * @returns Objective coordinates
 */
function resolveObjective(radarScan: RadarScan): Coordinates {
  // Filter
  let scans = radarScan.scan
    // Filter out coordinates that are more than 100 units away
    .filter(scan =>
      distanceLessThan(scan.coordinates),
    );

  if (radarScan.protocols.includes(Protocol.avoidMech)) {
    // Filter out coordinates with a mech
    scans = scans.filter(scan => !hasMech(scan));
  }

  if (radarScan.protocols.includes(Protocol.avoidCrossfire)) {
    // Filter out coordinates with allies
    scans = scans.filter(scan => !hasAllies(scan));
  }

  // Sort
  if (radarScan.protocols.includes(Protocol.closestEnemies)) {
    scans = sortByDistanceAsc(scans);
  }

  if (radarScan.protocols.includes(Protocol.furthestEnemies)) {
    scans = sortByDistanceDesc(scans);
  }

  // Prioritize
  if (
    radarScan.protocols.includes(Protocol.prioritizeMech)
    && radarScan.protocols.includes(Protocol.assistAllies)
  ) {
    const objective = scans.find(scan =>
      hasMech(scan) && hasAllies(scan),
    );

    if (objective) {
      return objective.coordinates;
    }
  }

  if (radarScan.protocols.includes(Protocol.prioritizeMech)) {
    const objective = scans.find(scan =>
      hasMech(scan),
    );

    if (objective) {
      return objective.coordinates;
    }
  }

  if (radarScan.protocols.includes(Protocol.assistAllies)) {
    const objective = scans.find(scan => hasAllies(scan));

    if (objective) {
      return objective.coordinates;
    }
  }

  // Default: return the first scan coordinates
  return scans[0].coordinates;
}

export {
  Protocol,
  EnemyType,
  Enemy,
  Coordinates,
  Scan,
  RadarScan,
  resolveObjective,
};
