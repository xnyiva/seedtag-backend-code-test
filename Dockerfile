FROM node:lts-slim

# Use ENV=production to avoid installing devDependecies
ARG ENV=production
ARG NODE_PORT=9091
ARG NODE_DEBUG_PORT=9292
ARG NODE_AUTH_TOKEN

ENV NODE_ENV=${ENV}
ENV NODE_PORT=9091
ENV NODE_DEBUG_PORT=9292

# Add libcurl4 for mongodb-memory-server
RUN export DEBIAN_FRONTEND=noninteractive \
  # Get some security updates at the same time
  && apt-get update \
  && apt-get upgrade -y \
  # Get libcurl4
  && apt-get install -y --no-install-recommends libcurl4 \
  # Clean up everything
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Don't run as root
WORKDIR /home/node
USER node

# Install dependencies
COPY package.json ./package.json
COPY package-lock.json ./package-lock.json
COPY .npmrc ./.npmrc
RUN npm ci

# Copy source code after installing dependencies
COPY --chown=node:node . ./

EXPOSE ${NODE_PORT}

# Allow debugging
EXPOSE ${NODE_DEBUG_PORT}
VOLUME [ "/home/node" ]

CMD [ "./entrypoint.sh" ]