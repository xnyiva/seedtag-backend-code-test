seedtag-backend-code-test

# seedtag-backend-code-test

## Table of contents

### Functions

- [default](API.md#default)

## Functions

### default

▸ **default**(): `Promise`<`Server`\>

Starts the server

**`Throws`**

An error if the server fails to start

#### Returns

`Promise`<`Server`\>

A promise that resolves to the server instance

#### Defined in

index.ts:39
