# Seedtag Code Test

## Stack

### Framework

I needed something lightwheight and flexible enought to iterate quickly and be
able to change the plan if needed.

I chose [KoaJS](https://koajs.com/) as I have a good understanding on how it
works.

The way [KoaJS](https://koajs.com/) middleware functions work allow me to
easily handle errors on a single point.

Also allows an easy way to do a full task in the same midldleware function.
Even when there is a need for the rest of the middleware chain to finish
execution first. And example of this is the error handler middleware or the
request logger.

### Data storage

In this exercise no data storage was required, but as I'm using my own project
template, I just commented out the statements related to database usage,
here is my explanation of how it should work.

To store the data I usually use [mongoose](https://mongoosejs.com/) and
[mongo-memory-server](https://github.com/nodkz/mongodb-memory-server) as it
allows to use the same code for testing without the need of complex mocks.

Also is really easy to iterate to a full [MongoDB](https://www.mongodb.com/)
database if this was a real project.

### Data validation

Any incoming data is validated using [JSON Schemas](https://json-schema.org/)
and [AJV](https://ajv.js.org/).

Also [mongoose](https://mongoosejs.com/) provides all the validation we need
on the storage layer.

## Project structure

The files are organized in the following structure:

```sh
├── build             # The transpiled files from Typescript
├── docs              # Additional docs generated by TypeDoc
└── src               # All the application source code
    ├── lib           # Helper functions
    ├── middlewares   # Generic middlewares
    ├── models        # Database models
    └── routes        # Routes of the API
        └── radar    # All the endpoints related to the radar        
Dockerfile            # Configuration files
README.md
entrypoint.sh
jest.config.js
package-lock.json
package.json
tsconfig.json
```

All generic middlewares can be easily moved to its own module if there is
the need to use them on another project. The same goes for the routes.

## Testing

All the testing is done using [JEST](https://jestjs.io/).

Every **test file** sits side by side of the **code file** that intends to test
. This way is very clear where you can find what is failing in case any test
does not pass.

Every middleware function is tested individually.

Also [SuperTest](https://github.com/ladjs/supertest#readme) is used on top of
[JEST](https://jestjs.io/) to test the full middleware chain of every endpoint.

[mongo-memory-server](https://github.com/nodkz/mongodb-memory-server) is used to
ensure that the code will work with a real database.

## Loose ends

There are still some things I would have done if I had more time to invest in
the challenge:

* Document the endpoints using OpenAPI and mounting the generated docs on the app.
* Fix the TypeDoc not generating the docs.
* Put the generic middlewares in modules and use the Gitlab package repository to install them.
* Create different Dockerfiles for development, testing and production.
* Setup CI jobs to build everything.
