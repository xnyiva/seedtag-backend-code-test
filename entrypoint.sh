#!/bin/sh

# Exit the script on any error
set -e

if [ "$NODE_ENV" = "development" ]; then
  npm run dev
else
  if [ "$NODE_ENV" = "testing" ]; then
    npm run test
  else
    npm start
  fi
fi